const calendarConfig = {
    container: 'calendar',
    months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
    weekDays: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
    monthDays: 31,
}

const calendar = new Calendar(calendarConfig);
